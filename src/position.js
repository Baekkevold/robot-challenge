import { emptySquare, floor, stringify } from './utils'

/**
 * function triggered by action command forward or backwards
 * @param {String} command 'f' | 'b'
 * @param {Object} state current state
 * @returns {Object} next robot state
 */
export const move = (command, state) => {
  const nextPosition = getPosition(command, state.robot)

  if (!emptySquare(nextPosition, state)) {
    console.warn(`Abort! Robot will collide at: ${stringify(nextPosition)}`)
    return state
  } 

  return {
    robot: {
      ...state.robot,
      coordinates: nextPosition,
    }
  }
}

const getPosition = (command, { coordinates, direction }) => {
  const degrees = calculateDegrees(command, direction.degrees)
  
  return positions(coordinates)[degrees]
}

const positions = ({ x, y }) => ({
  0: { x, y: y - 1 },
  90: { x: x + 1, y },
  180: { x, y: y + 1 },
  270: { x: x - 1, y },
})

// If moving backwards we need to rotate 180 degrees
const calculateDegrees = (command, degrees) =>
  floor(degrees + (command === 'b' ? 180 : 0))

import { setGrid } from './grid'
import { turn } from './direction'
import { toggleObstacle } from './obstacle'
import { move } from './position'

const initialState = {
  grid: {
    x: 10,
    y: 10,
  },
  obstacles: [],
  robot: {
    coordinates: {
      x: 0,
      y: 0,
    },
    direction: {
      cardinal: 's',
      degrees: 180,
    },
  },
}

export const state = initialState

const command = c => state => {
  const commands = {
    b: move,
    f: move,
    l: turn,
    r: turn,
    default: val => {
      console.warn(`${val} is not a valid command`)
      return state
    },
  }

  return (commands[c] || commands.default)(c, state)
}

/**
 * Reset to initial state but with new grid size
 * @param {Number} size
 * @returns {Function(Object): Object}
 */
const reset = size => ({ grid }) => ({
  ...initialState,
  grid: setGrid(size, state.grid),
})

export const actions = {
  command,
  reset,
  toggleObstacle,
}

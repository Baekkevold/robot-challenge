/**
 * Check if square if empty
 * @param {Object} position {x, y} 
 * @param {Object} state root state object
 * @returns {Boolean} 
 */
export const emptySquare = (position, { grid, obstacles, robot }) =>
  inGrid(position, grid) &&
  !sameCoordinates(position, robot.coordinates) &&
  !obstacles.find(obstacle =>
      sameCoordinates(position, obstacle.position))

export const floor = nr =>
  (nr + 360) % 360

const inGrid = ({ x, y }, grid) =>
  Number.isInteger(x) &&
  Number.isInteger(y) &&
  x >= 0 &&
  y >= 0 &&
  x < grid.x &&
  y < grid.y

export const sameCoordinates = (a, b) =>
  a.x === b.x &&
  a.y === b.y

export const stringify = val =>
  JSON.stringify(val, null, 2)

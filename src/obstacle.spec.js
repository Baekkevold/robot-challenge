import { addObstacle, removeObstacle, toggleObstacle } from './obstacle'
import { state } from './store'

describe('obstacle', () => {
  it('should add a new obstacle if the coordinate is empty', () => {
    const a = toggleObstacle({ x: 1, y: 1 })(state)
    expect(a.obstacles.length).toBe(1)

    const newState = { ...state, obstacles: a.obstacles }

    const b = toggleObstacle({ x: 1, y: 1 })(newState)
    expect(a.obstacles.length).toBe(1)
  })

  it('should remove obstacle by coordinate', () => {
    const a = toggleObstacle({ x: 1, y: 1 })(state)
    expect(a.obstacles.length).toBe(1)

    const newState = { ...state, obstacles: a.obstacles }

    const b = toggleObstacle({ x: 0, y: 0 })(newState)
    expect(a.obstacles.length).toBe(1)

    const c = toggleObstacle({ x: 1, y: 1 })(newState)
    expect(c.obstacles.length).toBe(0)
  })
})

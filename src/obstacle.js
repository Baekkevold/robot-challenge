import uuid from 'uuid'
import { emptySquare, sameCoordinates, stringify } from './utils'

/**
 * Toggle obstacle action (add or remove)
 * @param {Object} position { x, y }
 * @returns {Function(Object): Object} new obstacle state
 */
export const toggleObstacle = position => state =>
  emptySquare(position, state)
    ? add(position, state)
    : remove(position, state)

const add = (position, { obstacles }) => ({
  obstacles: [
    ...obstacles,
    getObstacleObject(position, name),
  ],
})

const remove = (position, { obstacles }) => ({
  obstacles: obstacles.filter(x =>
    !sameCoordinates(position, x.position))
})

const getObstacleObject = position => ({
  id: uuid(),
  position,
})

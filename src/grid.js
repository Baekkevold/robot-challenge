export const minSize = 2
export const maxSize = 100

/**
 * Set grid
 * @param {Number} size
 * @returns {Object} new grid state
 */
export const setGrid = (size, state) => {
  if (!isValid(size)) {
    console.warn(`${size} is not valid! Grid size must be a Integer between ${minSize} and ${maxSize}`)
    return state
  }
  // Limiting this to a square for now
  return {
    x: size,
    y: size
  }
}

const isValid = (size) =>
  Number.isInteger(size) &&
  size >= minSize &&
  size <= maxSize

import { getByCardinal, turn } from './direction'
import { state } from './store'

const getCoo = (x, y) => ({ x, y })
const getDir = (degrees, cardinal) =>
  ({ cardinal, degrees })

const getPos = (command, degrees, cardinal) =>
  turn(command, {
    ...state,
    robot: {
      coordinates: getCoo(0, 0),
      direction: getDir(degrees, cardinal),
    }
  })

describe('direction', () => {
  it('should return direction object based on cardinal direction', () => {
    expect(getByCardinal('n')).toEqual(getDir(0, 'n'))
    expect(getByCardinal('e')).toEqual(getDir(90, 'e'))
    expect(getByCardinal('s')).toEqual(getDir(180, 's'))
    expect(getByCardinal('w')).toEqual(getDir(270, 'w'))
    expect(getByCardinal()).toEqual(undefined)
    expect(getByCardinal('invalid')).toEqual(undefined)
  })

  describe('turn', () => {
    it('should return the new position object without changed coordinates', () => {
      // Turning left
      expect(getPos('l', 0, 'n').robot.coordinates).toEqual({ x: 0, y: 0 })
      expect(getPos('l', 90, 'e').robot.coordinates).toEqual({ x: 0, y: 0 })
      expect(getPos('l', 180, 's').robot.coordinates).toEqual({ x: 0, y: 0 })
      expect(getPos('l', 270, 'w').robot.coordinates).toEqual({ x: 0, y: 0 })

      // Turning right
      expect(getPos('r', 0, 'n').robot.coordinates).toEqual({ x: 0, y: 0 })
      expect(getPos('r', 90, 'e').robot.coordinates).toEqual({ x: 0, y: 0 })
      expect(getPos('r', 180, 's').robot.coordinates).toEqual({ x: 0, y: 0 })
      expect(getPos('r', 270, 'w').robot.coordinates).toEqual({ x: 0, y: 0 })
    })

    it('should return the new position object with updated direction', () => {
      // Turning left
      expect(getPos('l', 0, 'n').robot.direction).toEqual({ cardinal: 'w', degrees: 270 })
      expect(getPos('l', 90, 'e').robot.direction).toEqual({ cardinal: 'n', degrees: 0 })
      expect(getPos('l', 180, 's').robot.direction).toEqual({ cardinal: 'e', degrees: 90 })
      expect(getPos('l', 270, 'w').robot.direction).toEqual({ cardinal: 's', degrees: 180 })

      // Turning right
      expect(getPos('r', 0, 'n').robot.direction).toEqual({ cardinal: 'e', degrees: 90 })
      expect(getPos('r', 90, 'e').robot.direction).toEqual({ cardinal: 's', degrees: 180 })
      expect(getPos('r', 180, 's').robot.direction).toEqual({ cardinal: 'w', degrees: 270 })
      expect(getPos('r', 270, 'w').robot.direction).toEqual({ cardinal: 'n', degrees: 0 })
    })
  })
})

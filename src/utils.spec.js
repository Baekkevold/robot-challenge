import { emptySquare } from './utils'
import { state } from './store'

describe('utils', () => {
  describe('emptySquare', () => {
    it('should be inside the grid', () => {
      const getState = grid => ({ ...state, grid })
      const grid = { x: 10, y: 5 }

      const a = emptySquare({x: -1, y: 3 }, getState(grid))
      expect(a).toBe(false)

      const b = emptySquare({x: 3, y: -1 }, getState(grid))
      expect(b).toBe(false)

      const c = emptySquare({x: 10, y: 0 }, getState(grid))
      expect(c).toBe(false)

      const d = emptySquare({x: 3, y: 5 }, getState(grid))
      expect(d).toBe(false)

      const e = emptySquare({x: 0, y: 4 }, getState(grid))
      expect(e).toBe(true)

      const f = emptySquare({x: 9, y: 0 }, getState(grid))
      expect(f).toBe(true)

      const g = emptySquare({x: 1, y: 1 }, getState(grid))
      expect(g).toBe(true)
    })

    it('should not be on the same coordinates as the robot', () => {
      const grid = { x: 10, y: 5 }
      const getState = coordinates => ({ ...state, grid, robot: {
        direction: state.robot.direction,
        coordinates,
      }})

      const a = emptySquare({x: 1, y: 0 }, getState({ x: 0, y: 0 }))
      expect(a).toBe(true)

      const b = emptySquare({x: 0, y: 1 }, getState({ x: 1, y: 0 }))
      expect(b).toBe(true)

      const c = emptySquare({x: 9, y: 4 }, getState({x: 9, y: 4 }))
      expect(c).toBe(false)

      const d = emptySquare({x: 0, y: 1 }, getState({x: 0, y: 1 }))
      expect(d).toBe(false)
    })

    it('should not be on the same coordinates as any obstacle', () => {
      const grid = { x: 10, y: 5 }
      const getState = position => ({ ...state, grid, obstacles: [{
        id: 1337,
        name: 'Hindret',
        position,
      }]})

      const a = emptySquare({x: 1, y: 0 }, getState({ x: 0, y: 0 }))
      expect(a).toBe(true)

      const b = emptySquare({x: 0, y: 1 }, getState({ x: 1, y: 0 }))
      expect(b).toBe(true)

      const c = emptySquare({x: 9, y: 4 }, getState({x: 9, y: 4 }))
      expect(c).toBe(false)

      const d = emptySquare({x: 0, y: 1 }, getState({x: 0, y: 1 }))
      expect(d).toBe(false)
    })
  })
})
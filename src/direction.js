import { floor } from './utils'

const directions = [
  { cardinal: 'n', degrees: 0 },
  { cardinal: 'e', degrees: 90 },
  { cardinal: 's', degrees: 180 },
  { cardinal: 'w', degrees: 270 },
]

/**
 * Turn action triggered on left or right
 * @param {String} command 'l' | 'r' 
 * @param {Object} state current state
 * @returns {Object} next robot state
 */
export const turn = (command, { robot }) => ({
  robot: {
    ...robot,
    direction: getDirection(command, robot),
  }
})

/**
 * @param {String} cardinal 'n' | 'e' | 's' | 'w' 
 * @returns {Object} { cardinal, degrees }
 */
export const getByCardinal = cardinal =>
  directions.find(x => x.cardinal === cardinal)

const getDirection = (command, { direction }) =>
  getByDegrees(addOrRemove(command, direction))

const getByDegrees = val =>
  directions.find(x => x.degrees === floor(val))

const addOrRemove = (command, { degrees }) =>
  command === 'l'
    ? degrees - 90
    : degrees + 90

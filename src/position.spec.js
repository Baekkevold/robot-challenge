import { move } from './position'
import { state } from './store'

describe('position', () => {
  it('should calculate and return the new position', () => {
    expect(move('f', state).robot.coordinates).toEqual({ x: 0, y: 1 })
  })

  it('should return the current position if next is a collision', () => {
    expect(move('b', state).robot.coordinates).toEqual({ x: 0, y: 0 })
  })
})

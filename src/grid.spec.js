import { setGrid } from './grid'
import { state } from './store'

describe('grid', () => {
  it('should ignore invalid inputs', () => {
    expect(setGrid(undefined, state.grid)).toEqual(state.grid)
    expect(setGrid(null, state.grid)).toEqual(state.grid)
    expect(setGrid(-2, state.grid)).toEqual(state.grid)
    expect(setGrid(1, state.grid)).toEqual(state.grid)
    expect(setGrid(13.37, state.grid)).toEqual(state.grid)
    expect(setGrid(NaN, state.grid)).toEqual(state.grid)
    expect(setGrid('10', state.grid)).toEqual(state.grid)
    expect(setGrid(0, state.grid)).toEqual(state.grid)
  })

  it('should return a new grid object on valid input', () => {
    expect(setGrid(2, state.grid)).toEqual({ x: 2, y: 2 })
    expect(setGrid(100, state.grid)).toEqual({ x: 100, y: 100 })
    expect(setGrid(5, state.grid)).toEqual({ x: 5, y: 5 })
  })
})

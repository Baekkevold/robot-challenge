import { h } from 'hyperapp'

export const tileSize = 50

const tileStyle = ({
  height: tileSize + 'px',
  width: tileSize + 'px',
})

const robotStyle = degrees => ({
  transform: `rotate(${degrees - 90}deg)`
})

const Robot = ({ direction }) => (
  <div style={robotStyle(direction.degrees)} class="robot">➔</div>
)

const Obstacle = () => <div class="obstacle"></div>

export const Tile = ({ hasObstacle, hasRobot, position, robot, toggle }) => (
  <div
    onclick={() => toggle(position)}
    class="grid__tile"
    style={tileStyle}
  >
    {hasRobot && <Robot {...robot} />}
    {hasObstacle && <Obstacle />}
  </div>
)

import { h } from 'hyperapp'
import { Tile, tileSize } from './Tile'
import { sameCoordinates } from '../utils'

const loop = size => [...Array(size).keys()]

const px = val => val + 'px'

const gridStyle = ({ x, y }) => ({
  height: px(y * tileSize),
  width: px(x * tileSize),
  transform: `perspective(${px(x * tileSize)}) rotateX(45deg)`,
})

export const Grid = ({ grid, obstacles, robot, toggle }) => (
  <div
    class='grid'
    style={gridStyle(grid)}
  >
    {loop(grid.y).map(y =>
      loop(grid.x).map(x =>
        <Tile
          hasObstacle={obstacles.find(obstacle => sameCoordinates({ x, y }, obstacle.position))}
          hasRobot={sameCoordinates({ x, y }, robot.coordinates)}
          robot={robot}
          position={{ x, y }}
          toggle={toggle}
        />
      )
    )}
  </div>
)

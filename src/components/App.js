import { h } from 'hyperapp'
import { Description } from './Description'
import { Grid } from './Grid'
import { Logo } from './Logo'
import { RangePicker } from './RangePicker'

import '../assets/styles.css'

export const App = (state, actions) => (
  <div id="app">
    <div class="top">
      <Logo />
      <Description />
      <RangePicker reset={actions.reset} size={state.grid.x} />
    </div>
    <Grid {...state} toggle={actions.toggleObstacle} />
  </div>
)

import { h } from 'hyperapp'

export const Logo = () => (
  <h1 class="logo">
    <span class="logo__body">ROBOT CHALLENGE</span>
  </h1>
)
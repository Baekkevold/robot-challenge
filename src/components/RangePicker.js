import { h } from 'hyperapp'

export const RangePicker = ({ reset, size }) => (
  <input
    class="range"
    type="range"
    min="2"
    max="30"
    value={size}
    onchange={({ target }) => reset(parseInt(target.value))}
  />
)
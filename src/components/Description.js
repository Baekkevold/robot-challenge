import { h } from 'hyperapp'

export const Description = () => {
  const text = 'Control the robot with the arrow keys ⇦ ⇨ ⇧ ⇩. Adjust the grid size with the range slider below. Click on squares to add or remove obstacles.'
  return <p class="description">{text}</p>
}
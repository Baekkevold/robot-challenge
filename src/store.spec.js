import { actions, state } from './store'

describe('store actions', () => {
  describe('command', () => {
    it('should ignore invalid commands', () => {
      expect(actions.command()(state)).toEqual(state)
      expect(actions.command(null)(state)).toEqual(state)
      expect(actions.command(2)(state)).toEqual(state)
      expect(actions.command('F')(state)).toEqual(state)
    })

    it('should act on valid commands', () => {
      expect(actions.command('f')(state)).not.toEqual(state)
      expect(actions.command('l')(state)).not.toEqual(state)
      expect(actions.command('r')(state)).not.toEqual(state)
    })
  })
})
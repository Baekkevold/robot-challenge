# Robot Challenge 🤖

*Trying out the functional super minimalistic library [Hyperapp](https://hyperapp.js.org/) and the zero config alternative to Webpack [Parcel](https://parceljs.org/).*

## Prerequisities

* Node >= 8
* Npm >= 5.3

## Install

```
$ npm i
```

## Run

```
$ npm start
```

## Build

```
$ npm run build
```

## Tests

Tests written in [Jest](https://facebook.github.io/jest/).

Run:
```
$ npm test
```

or watch:

```
$ npm run test:watch
```

With test coverage:

```
$ npm run test:coverage
```

import { app, h } from 'hyperapp'
import { withLogger } from '@hyperapp/logger'
import { actions, state } from './src/store'
import { App } from './src/components/App'

export const main = withLogger(app)(state, actions, App, document.body)

// Register keydown event
document.onkeydown = e => {
  switch (e.keyCode) {
    case 37: return main.command('l')
    case 38: return main.command('f')
    case 39: return main.command('r')
    case 40: return main.command('b')
  }
}
